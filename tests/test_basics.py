"""
Basic unit tests.

Artem Basalaev <artem[dot]basalaev[at]pm.me>
Christian Darsow-Fromm <cdarsowf[at]physnet.uni-hamburg.de>
"""

import unittest
import spicypy


class Test(unittest.TestCase):
    def setUp(self):
        pass

    def test_version(self):
        spicypy.__version__

    def test_module_access(self):
        spicypy.control
        spicypy.signal

    def test_short_class_access(self):
        spicypy.signal.FrequencySeries
        spicypy.signal.TimeSeries
        spicypy.signal.TransferFunction
        spicypy.control.System
