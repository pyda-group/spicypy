"""
Class for 'signal' module unit tests

Artem Basalaev <artem[dot]basalaev[at]pm.me>,
Christian Darsow-Fromm <cdarsowf[at]physnet.uni-hamburg.de>,
Octavio Vega <vega00087[at]gmail[dot]com>,
Shreevathsa Chalathadka Subrahmanya <schalath[at]physnet.uni-hamburg.de>
"""

import unittest
import numpy as np

from gwpy.timeseries import TimeSeriesDict
from astropy.units import UnitConversionError

from spicypy.signal.frequency_series import FrequencySeries
from spicypy.signal.time_series import TimeSeries
from spicypy.signal.spectrogram import Spectrogram
from spicypy.signal.transfer_function import TransferFunction
from spicypy.signal.coherent_subtraction import coherent_subtraction
from spicypy.signal.wiener_filter import WienerFilter


def _max_value_in_window(
    values, frequencies, input_frequency, window_size=100, find_min=False
):
    freq_window = np.where(abs(frequencies - input_frequency) < window_size)
    func = np.argmax
    if find_min:
        func = np.argmin

    value_index = func(abs(values[freq_window[0][0] : freq_window[0][-1] + 1]))
    value_index = value_index + freq_window[0][0]
    return values[value_index]


class Test(unittest.TestCase):
    def setUp(self):
        # define two waves, each with two frequencies, one of which is the same (f1)
        # set parameters such that different averaging methods are comparable
        n_samples = 9996
        total_time = 1  # in seconds
        self.sample_rate = n_samples / total_time
        true_fft_resolution = n_samples / self.sample_rate
        daniell_number_averages = 51  # bigger means worse resolution!
        welch_n_samples_per_segment = (n_samples / true_fft_resolution) * (
            1 / daniell_number_averages
        )  # bigger means better resolution!
        fftlength = welch_n_samples_per_segment / n_samples

        self.f1 = 1000
        self.f2 = 300
        self.f3 = 700
        a1 = 100
        a2 = 10

        t = np.linspace(0, 1, n_samples)
        t_fast = np.linspace(0, 1, 2 * n_samples)
        np.random.seed(0)
        self.s1 = (
            a1 * np.sin(2 * np.pi * self.f1 * t)
            + a2 * np.sin(2 * np.pi * self.f2 * t)
            + np.random.normal(0, scale=10, size=n_samples)
        )
        self.s2 = (
            a1 * np.sin(2 * np.pi * self.f1 * t)
            + a2 * np.sin(2 * np.pi * self.f3 * t)
            + np.random.normal(0, scale=10, size=n_samples)
        )
        self.s3 = (
            a1 * np.sin(2 * np.pi * self.f1 * t)
            + a2 * np.sin(2 * np.pi * self.f2 * t)
            + a2 * np.sin(2 * np.pi * self.f3 * t)
            + np.random.normal(0, scale=10, size=n_samples)
        )
        self.s1_complex = (
            a1 * np.sin(2 * np.pi * self.f1 * t)
            + 1j * a2 * np.sin(2 * np.pi * self.f2 * t)
            + np.random.normal(0, scale=10, size=n_samples)
        )
        self.s2_complex = (
            a1 * np.sin(2 * np.pi * self.f1 * t)
            + 1j * a2 * np.sin(2 * np.pi * self.f3 * t)
            + np.random.normal(0, scale=10, size=n_samples)
        )
        self.s_rnd = np.random.normal(0, scale=10, size=n_samples)
        self.s_rnd_complex = np.random.normal(
            0, scale=10, size=n_samples
        ) + 1j * np.random.normal(0, scale=10, size=n_samples)
        self.s_int = np.random.randint(-1000, 1000, n_samples)
        self.averaging_kwargs = {
            "welch": {"fftlength": fftlength},
            "lpsd": {"method": "lpsd"},
            "daniell": {
                "method": "daniell",
                "number_averages": daniell_number_averages,
            },
        }
        self.units = ["m", "m/s", "m/s2", "1", "Hz", "V"]

        # generate Wiener filter test signals
        total_time = (
            1500  # in seconds, enough to generate WF with 15 000 taps and validate it
        )
        self.wf_sampling_frequency = 100  # in Hz
        n_samples = total_time * self.wf_sampling_frequency
        total_time += (
            1.0 / self.wf_sampling_frequency
        )  # need an extra sample to create WF with 15 000 taps
        f1 = 3  # first frequency in Hz
        f2 = 9.5  # second frequency in Hz
        a1 = 5.0
        a2 = 0.5
        noise_scale = 0.05
        np.random.seed(777)
        t = np.linspace(0, total_time, n_samples)

        # Generate target: Signal2 - these time series will be inferred from references. Add two frequencies and a
        # bit of white noise
        self.wf_s2 = (
            3 * a1 * np.sin(2 * np.pi * f1 * t)
            + 2 * a2 * np.sin(2 * np.pi * f2 * t + 0.1 * np.pi)
            + np.random.normal(0, scale=noise_scale, size=n_samples)
        )

        # Generate one reference with both frequencies: Signal1, at different amplitudes and with some uncorrelated
        # white noise
        self.wf_s1 = (
            a1 * np.sin(2 * np.pi * f1 * t)
            + a2 * np.sin(2 * np.pi * f2 * t)
            + np.random.normal(0, scale=noise_scale, size=n_samples)
        )

        # Generate two references, Signal 11 and Signal 12, where each has only ONE signal frequency (with some
        # uncorrelated white noise)
        self.wf_s11 = a1 * np.sin(2 * np.pi * f1 * t) + np.random.normal(
            0, scale=noise_scale, size=n_samples
        )
        self.wf_s12 = a2 * np.sin(2 * np.pi * f2 * t) + np.random.normal(
            0, scale=noise_scale, size=n_samples
        )
        self.default_wf = None

        n_samples = 9996
        self.s2_fast = (
            a1 * np.sin(2 * np.pi * self.f1 * t_fast)
            + a2 * np.sin(2 * np.pi * self.f3 * t_fast)
            + np.random.normal(0, scale=10, size=2 * n_samples)
        )

    def test_time_series_creation(self):
        a = [2, 2, 3, -1, 5]
        t = [0, 1, 2, 3, 4]

        ts = TimeSeries(a, times=t)

        np.testing.assert_array_equal(ts.value, np.array(a))
        np.testing.assert_array_equal(ts.times.value, np.array(t))
        self.assertEqual(ts.times.unit, "s")

    def test_frequency_series_creation(self):

        a = [2, 2, 3, -1, 5]
        f = [0, 1, 2, 3, 4]

        FrequencySeries(a, frequencies=f)

    def test_psd(self):
        max_psd_method = {"welch": 77.19, "lpsd": 59.23, "daniell": 99.00}
        for method in self.averaging_kwargs:
            s1 = TimeSeries(
                self.s1, sample_rate=self.sample_rate, name="Signal 1", unit="meter"
            )
            psd = s1.psd(**self.averaging_kwargs[method])
            self.assertIsInstance(psd, FrequencySeries)
            max_psd = _max_value_in_window(psd.value, psd.frequencies.value, self.f1)
            self.assertAlmostEqual(max_psd, max_psd_method[method], delta=1)

    def test_psd_int(self):
        max_psd_method = {"welch": 82.7, "lpsd": 92.85, "daniell": 87.93}
        for method in self.averaging_kwargs:
            s1 = TimeSeries(
                self.s_int, sample_rate=self.sample_rate, name="Signal 1", unit="meter"
            )
            psd = s1.psd(**self.averaging_kwargs[method])
            self.assertIsInstance(psd, FrequencySeries)
            max_psd = _max_value_in_window(psd.value, psd.frequencies.value, self.f1)
            self.assertAlmostEqual(max_psd, max_psd_method[method], delta=1)

    def test_psd_hamm(self):
        max_psd_method = {"welch": 81.17, "lpsd": 123.80, "daniell": 99.00}
        for method in self.averaging_kwargs:
            s1 = TimeSeries(
                self.s1, sample_rate=self.sample_rate, name="Signal 1", unit="meter"
            )
            psd = s1.psd(window="hamm", **self.averaging_kwargs[method])
            self.assertIsInstance(psd, FrequencySeries)
            max_psd = _max_value_in_window(psd.value, psd.frequencies.value, self.f1)
            self.assertAlmostEqual(max_psd, max_psd_method[method], delta=1)

    def test_asd(self):
        max_asd_method = {"welch": 8.78, "lpsd": 7.69, "daniell": 9.94}
        for method in self.averaging_kwargs:
            s1 = TimeSeries(
                self.s1, sample_rate=self.sample_rate, name="Signal 1", unit="meter"
            )
            asd = s1.asd(**self.averaging_kwargs[method])
            self.assertIsInstance(asd, FrequencySeries)
            max_asd = _max_value_in_window(asd.value, asd.frequencies.value, self.f1)
            self.assertAlmostEqual(max_asd, max_asd_method[method], delta=1)

    def test_csd(self):
        max_csd_method = {"welch": 53.78, "lpsd": 59.17, "daniell": 98.75}
        for method in self.averaging_kwargs:
            s1 = TimeSeries(
                self.s1, sample_rate=self.sample_rate, name="Signal 1", unit="meter"
            )
            s2 = TimeSeries(
                self.s2, sample_rate=self.sample_rate, name="Signal 2", unit="watt"
            )
            csd = s1.csd(s2, **self.averaging_kwargs[method])
            self.assertIsInstance(csd, FrequencySeries)
            max_csd = _max_value_in_window(
                np.abs(csd.value), csd.frequencies.value, self.f1
            )
            self.assertAlmostEqual(max_csd, max_csd_method[method], delta=1)

    def test_daniell_csd_logarithmic_binning(self):
        s1 = TimeSeries(
            self.s1, sample_rate=self.sample_rate, name="Signal 1", unit="meter"
        )
        s2 = TimeSeries(
            self.s2, sample_rate=self.sample_rate, name="Signal 2", unit="watt"
        )
        csd = s1.csd(s2, method="daniell", number_averages=51, binning="logarithmic")
        self.assertIsInstance(csd, FrequencySeries)
        # check f1 - the common frequency
        max_csd = _max_value_in_window(
            np.abs(csd.value), csd.frequencies.value, self.f1
        )
        self.assertAlmostEqual(max_csd, 96.83, delta=1.0)

    def test_daniell_csd_median(self):
        s1 = TimeSeries(
            self.s1, sample_rate=self.sample_rate, name="Signal 1", unit="meter"
        )
        s2 = TimeSeries(
            self.s2, sample_rate=self.sample_rate, name="Signal 2", unit="watt"
        )
        csd = s1.csd(
            s2,
            method="daniell",
            average="median",
            number_averages=3,
        )
        csd_default = s1.csd(
            s2,
            method="daniell",
            number_averages=11,
        )
        self.assertIsInstance(csd, FrequencySeries)
        # check f1 - the common frequency
        max_csd = _max_value_in_window(
            np.abs(csd.value), csd.frequencies.value, self.f1
        )
        max_csd_default = _max_value_in_window(
            np.abs(csd_default.value), csd_default.frequencies.value, self.f1
        )

        self.assertAlmostEqual(max_csd, max_csd_default, delta=300)

    def test_daniell_csd_invalid_average(self):

        a1 = [2, 2, 3, -1, 5]
        a2 = [4, 6, 8, -3, 6]
        t = [0, 1, 2, 3, 4]

        ts1 = TimeSeries(a1, times=t)
        ts2 = TimeSeries(a2, times=t)
        self.assertRaises(
            ValueError, ts1.csd, ts2, method="daniell", average="unicorns"
        )

    def test_daniell_csd_invalid_inputs(self):

        a1 = [2, 2, 3, -1, 5, 4]
        a2 = [4, 6, 8, -3, 6]
        t1 = [0, 1, 2, 3, 4, 5]
        t2 = [0, 1, 2, 3, 4]

        ts1 = TimeSeries(a1, times=t1)
        ts2 = TimeSeries(a2, times=t2)
        self.assertRaises(ValueError, ts1.csd, ts2, method="daniell")

    def test_daniell_csd_even_averages(self):

        a1 = [2, 2, 3, -1, 5]
        a2 = [4, 6, 8, -3, 6]
        t = [0, 1, 2, 3, 4]

        ts1 = TimeSeries(a1, times=t)
        ts2 = TimeSeries(a2, times=t)
        self.assertRaises(
            NotImplementedError, ts1.csd, ts2, method="daniell", number_averages=10
        )

    def test_daniell_csd_negative_averages(self):

        a1 = [2, 2, 3, -1, 5]
        a2 = [4, 6, 8, -3, 6]
        t = [0, 1, 2, 3, 4]

        ts1 = TimeSeries(a1, times=t)
        ts2 = TimeSeries(a2, times=t)
        self.assertRaises(
            ValueError, ts1.csd, ts2, method="daniell", number_averages=-9
        )

    def test_daniell_complex_csd(self):
        # Complex two-sided CSD output with complex inputs
        s1 = TimeSeries(
            self.s1_complex, sample_rate=self.sample_rate, name="Signal 1", unit="meter"
        )
        s2 = TimeSeries(
            self.s2_complex, sample_rate=self.sample_rate, name="Signal 2", unit="watt"
        )

        csd = s1.csd(s2, **self.averaging_kwargs["daniell"])
        self.assertIsInstance(csd, FrequencySeries)
        csd_default = s1.csd(s2, **self.averaging_kwargs["welch"])

        # check f1 - the common frequency
        max_csd = _max_value_in_window(
            np.abs(csd.value), csd.frequencies.value, self.f1
        )
        max_csd_default = _max_value_in_window(
            np.abs(csd_default.value), csd_default.frequencies.value, self.f1
        )
        self.assertAlmostEqual(max_csd, max_csd_default, delta=25)

        # check -f1
        max_csd = _max_value_in_window(
            np.abs(csd.value), csd.frequencies.value, -self.f1
        )
        max_csd_default = _max_value_in_window(
            np.abs(csd_default.value), csd_default.frequencies.value, -self.f1
        )
        self.assertAlmostEqual(max_csd, max_csd_default, delta=25)

    def test_gwpy_wrapping(self):
        s1 = TimeSeries(
            self.s1, sample_rate=self.sample_rate, name="Signal 1", unit="meter"
        )
        s2 = TimeSeries(
            self.s2, sample_rate=self.sample_rate, name="Signal 2", unit="watt"
        )

        self.assertRaises(NotImplementedError, s1.fft, method="lpsd")
        fft = s1.fft()
        self.assertIsInstance(fft, FrequencySeries)

        self.assertRaises(NotImplementedError, s1.average_fft, method="lpsd")
        avg_fft = s1.average_fft()
        self.assertIsInstance(avg_fft, FrequencySeries)

        avg_spectrogram = s1.spectrogram(0.1)
        self.assertIsInstance(avg_spectrogram, Spectrogram)

        self.assertRaises(NotImplementedError, s1.spectrogram2, 0.1, method="lpsd")
        spectrogram2 = s1.spectrogram2(0.1)
        self.assertIsInstance(spectrogram2, Spectrogram)

        self.assertRaises(NotImplementedError, s1.fftgram, 0.1, method="lpsd")
        fftgram = s1.fftgram(0.1)
        self.assertIsInstance(fftgram, Spectrogram)

        self.assertRaises(NotImplementedError, s1.spectral_variance, 0.1, method="lpsd")

        self.assertRaises(NotImplementedError, s1.rayleigh_spectrum, method="lpsd")
        rayleigh = s1.rayleigh_spectrum()
        self.assertIsInstance(rayleigh, FrequencySeries)

        self.assertRaises(NotImplementedError, s1.rayleigh_spectrogram, method="lpsd")
        rayleigh_spectrogram = s1.rayleigh_spectrogram(0.1)
        self.assertIsInstance(rayleigh_spectrogram, Spectrogram)

        csd_spectrogram = s1.csd_spectrogram(s2, 0.1)
        self.assertIsInstance(csd_spectrogram, Spectrogram)

        tf = s1.transfer_function(s2)
        self.assertIsInstance(tf, FrequencySeries)

        coh_spectrogram = s1.coherence_spectrogram(s2, 0.1)
        self.assertIsInstance(coh_spectrogram, Spectrogram)

    def test_transfer_function(self):

        s1 = TimeSeries(
            self.s1, sample_rate=self.sample_rate, name="Signal 1", unit="meter"
        )
        s2 = TimeSeries(
            self.s2, sample_rate=self.sample_rate, name="Signal 2", unit="watt"
        )
        s2_fast = TimeSeries(
            self.s2_fast,
            sample_rate=2 * self.sample_rate,
            name="Signal 2 fast",
            unit="watt",
        )

        for method in self.averaging_kwargs:
            tf = TransferFunction(s1, s2, **self.averaging_kwargs[method])
            tf_max = _max_value_in_window(
                np.abs(tf.tf.value), tf.tf.frequencies.value, self.f1
            )
            self.assertAlmostEqual(tf_max, 1.0, delta=0.3)
            # same signals, but different sampling rates (and different tf values because of resolution)
            if (
                method != "welch"
            ):  # our custom methods don't support differing sampling rates
                continue
            tf = TransferFunction(s1, s2_fast, **self.averaging_kwargs[method])
            tf_min = _max_value_in_window(
                np.abs(tf.tf.value), tf.tf.frequencies.value, self.f1, find_min=True
            )
            self.assertAlmostEqual(tf_min, 1.0e-3, delta=1.0e-3)

    def test_transfer_function_coherence(self):

        s1 = TimeSeries(
            self.s1, sample_rate=self.sample_rate, name="Signal 1", unit="meter"
        )
        s2 = TimeSeries(
            self.s2, sample_rate=self.sample_rate, name="Signal 2", unit="watt"
        )

        for method in self.averaging_kwargs:
            # common frequency - expect good coherence
            tf = TransferFunction(s1, s2, **self.averaging_kwargs[method])
            coh_max = _max_value_in_window(
                np.abs(tf.coherence.value), tf.coherence.frequencies.value, self.f1
            )
            self.assertAlmostEqual(coh_max, 1.0, delta=0.3)

            # non-common frequencies - expect poor coherence
            coh_max = _max_value_in_window(
                np.abs(tf.coherence.value), tf.coherence.frequencies.value, self.f2
            )
            self.assertAlmostEqual(coh_max, 0.0, delta=0.1)
            coh_max = _max_value_in_window(
                np.abs(tf.coherence.value), tf.coherence.frequencies.value, self.f3
            )
            self.assertAlmostEqual(coh_max, 0.0, delta=0.2)

    def test_transfer_function_psd2(self):

        s1 = TimeSeries(
            self.s1, sample_rate=self.sample_rate, name="Signal 1", unit="meter"
        )
        s2 = TimeSeries(
            self.s2, sample_rate=self.sample_rate, name="Signal 2", unit="watt"
        )
        max_psd2_method = {"welch": 76.95, "lpsd": 59.17, "daniell": 98.75}
        for method in self.averaging_kwargs:
            tf = TransferFunction(s1, s2, **self.averaging_kwargs[method])
            # frequency in signal, expect high value
            psd2_max = _max_value_in_window(
                np.abs(tf.psd2.value), tf.psd2.frequencies.value, self.f1
            )
            self.assertAlmostEqual(psd2_max, max_psd2_method[method], delta=0.3)

            # frequency not in signal, expect low value
            psd2_max = _max_value_in_window(
                np.abs(tf.psd2.value), tf.psd2.frequencies.value, self.f2
            )
            self.assertAlmostEqual(psd2_max, 0.0, delta=0.3)

    def test_invalid_transfer_function(self):

        a1 = [2, 2, 3, -1, 5]
        a2 = [4, 6, 8, -3, 6]
        t = [0, 1, 2, 3, 4]

        ts1 = TimeSeries(a1, times=t)
        ts2 = TimeSeries(a2, times=t)
        tf = TransferFunction(ts1, ts2)
        self.assertRaises(ValueError, TransferFunction, ts1, ts2, tf=tf.tf)
        self.assertRaises(ValueError, TransferFunction, ts1)

    def test_transfer_function_plot(self):

        s1 = TimeSeries(
            self.s1, sample_rate=self.sample_rate, name="Signal 1", unit="meter"
        )
        s2 = TimeSeries(
            self.s2, sample_rate=self.sample_rate, name="Signal 2", unit="watt"
        )

        tf = TransferFunction(s1, s2)
        tf.plot(coherence=True)

        # create transfer function from an existing one and try to plot
        TransferFunction(tf=tf).plot()

    def test_coherent_subtraction_psd(self):
        s1 = TimeSeries(
            self.s1, sample_rate=self.sample_rate, name="Signal 1", unit="meter"
        )
        s2 = TimeSeries(
            self.s2, sample_rate=self.sample_rate, name="Signal 2", unit="meter"
        )
        s3 = TimeSeries(
            self.s3, sample_rate=self.sample_rate, name="Signal 3", unit="meter"
        )
        s3_psd = s3.psd(**self.averaging_kwargs["daniell"])
        ts_dict = TimeSeriesDict()
        ts_dict["s1"] = s1
        ts_dict["s2"] = s2

        # first test for signals without names and units
        TimeSeries(self.s3).coherent_subtract(
            [TimeSeries(self.s1), TimeSeries(self.s2)],
            **self.averaging_kwargs["daniell"]
        )
        # now do for signals  with names and units
        s3_residual_psd = s3.coherent_subtract(
            ts_dict, **self.averaging_kwargs["daniell"]
        )
        # find amplitude at f2 in original psd
        psd_f2_max = _max_value_in_window(
            np.abs(s3_psd.value), s3_psd.frequencies.value, self.f2
        )
        # find amplitude at f3 in original psd
        psd_f3_max = _max_value_in_window(
            np.abs(s3_psd.value), s3_psd.frequencies.value, self.f3
        )
        self.assertAlmostEqual(psd_f2_max, 0.99, delta=0.01)
        self.assertAlmostEqual(psd_f3_max, 0.99, delta=0.01)

        # now same for residual psd - should be much smaller
        res_psd_f2_max = _max_value_in_window(
            np.abs(s3_residual_psd.value), s3_residual_psd.frequencies.value, self.f2
        )
        res_psd_f3_max = _max_value_in_window(
            np.abs(s3_residual_psd.value), s3_residual_psd.frequencies.value, self.f3
        )
        self.assertAlmostEqual(res_psd_f2_max, 0.05, delta=0.01)
        self.assertAlmostEqual(res_psd_f3_max, 0.04, delta=0.01)

    def test_coherent_subtraction_csd(self):
        # test stand-alone version of coherent subtraction from CSD:
        # supply random noise as ref and check that CSD is still ~same
        s1 = TimeSeries(
            self.s1, sample_rate=self.sample_rate, name="Signal 1", unit="meter"
        )
        s2 = TimeSeries(
            self.s2, sample_rate=self.sample_rate, name="Signal 2", unit="meter"
        )
        s_rnd = TimeSeries(
            self.s_rnd, sample_rate=self.sample_rate, name="Signal random", unit="meter"
        )
        # first test for signals without names and units
        coherent_subtraction(
            TimeSeries(self.s1),
            TimeSeries(self.s2),
            [TimeSeries(self.s_rnd)],
            **self.averaging_kwargs["daniell"]
        )
        # now do for signals  with names and units
        csd_residual = coherent_subtraction(
            s1, s2, [s_rnd], **self.averaging_kwargs["daniell"]
        )
        max_csd = _max_value_in_window(
            np.abs(csd_residual.value), csd_residual.frequencies.value, self.f1
        )
        self.assertAlmostEqual(max_csd, 98.75, delta=3)

    def test_coherent_subtraction_complex_csd(self):
        # test stand-alone version of coherent subtraction from CSD:
        # supply random noise as ref and check that CSD is still ~same
        # this time also with complex inputs
        s1 = TimeSeries(
            self.s1_complex, sample_rate=self.sample_rate, name="Signal 1", unit="meter"
        )
        s2 = TimeSeries(
            self.s2_complex, sample_rate=self.sample_rate, name="Signal 2", unit="meter"
        )
        s_rnd = TimeSeries(
            self.s_rnd_complex,
            sample_rate=self.sample_rate,
            name="Signal random",
            unit="meter",
        )
        csd_residual = coherent_subtraction(
            s1, s2, [s_rnd], **self.averaging_kwargs["daniell"]
        )
        max_csd_f1 = _max_value_in_window(
            np.abs(csd_residual.value), csd_residual.frequencies.value, self.f1
        )
        max_csd_minus_f1 = _max_value_in_window(
            np.abs(csd_residual.value), csd_residual.frequencies.value, -self.f1
        )
        self.assertAlmostEqual(max_csd_f1, 49.09, delta=1)
        self.assertAlmostEqual(max_csd_minus_f1, 49.09, delta=1)

    def test_frequency_series_units_conversion(self):
        # check that we can convert displacement to velocity and acceleration and various methods agree
        s1 = TimeSeries(
            self.s1, sample_rate=self.sample_rate, name="Signal 1", unit="cm"
        )
        elements_to_check = [0, 10, -1]

        asd_disp = s1.asd(**self.averaging_kwargs["lpsd"])
        asd_disp0 = asd_disp.times_iomega(0)
        asd_vel = asd_disp.to_velocity()
        asd_vel2 = asd_disp.times_iomega(1)
        asd_acc = asd_disp.to_acceleration()
        asd_acc2 = asd_disp.times_iomega(2)

        self.assertEqual(len(asd_acc), len(asd_acc2))
        self.assertEqual(len(asd_vel), len(asd_vel2))
        self.assertEqual(len(asd_disp), len(asd_disp0))
        for el in elements_to_check:
            self.assertAlmostEqual(asd_acc[el], asd_acc2[el], delta=0.001)
            self.assertAlmostEqual(asd_vel[el], asd_vel2[el], delta=0.001)
            self.assertAlmostEqual(asd_disp[el], asd_disp0[el], delta=0.001)

    def test_frequency_series_units_conversion_complex_daniell(self):
        # check with complex input and another averaging method
        s1 = TimeSeries(
            self.s1_complex,
            sample_rate=self.sample_rate,
            name="Signal 1",
            unit="km/s/s",
        )
        s2 = TimeSeries(
            self.s2_complex,
            sample_rate=self.sample_rate,
            name="Signal 2",
            unit="km/s/s",
        )
        elements_to_check = [0, 10, -1]
        csd = s1.csd(s2, **self.averaging_kwargs["daniell"])
        csd_disp = np.sqrt(csd).to_displacement()
        csd_disp2 = np.sqrt(csd).times_iomega(-2)
        self.assertEqual(len(csd_disp), len(csd_disp2))
        for el in elements_to_check:
            self.assertAlmostEqual(csd_disp[el], csd_disp2[el], delta=0.001)

    def test_frequency_series_units_conversion_wrong_units(self):
        # test wrong/missing units
        s1 = TimeSeries(
            self.s1, sample_rate=self.sample_rate, name="Signal 1", unit="W"
        )
        s2 = TimeSeries(self.s1, sample_rate=self.sample_rate, name="Signal 2")
        asd = s1.asd(**self.averaging_kwargs["lpsd"])
        asd2 = s2.asd(**self.averaging_kwargs["lpsd"])
        self.assertRaises(ValueError, asd.to_displacement)
        self.assertRaises(ValueError, asd.to_velocity)
        self.assertRaises(ValueError, asd.to_acceleration)
        self.assertRaises(ValueError, asd2.to_displacement)
        self.assertRaises(ValueError, asd2.to_velocity)
        self.assertRaises(ValueError, asd2.to_acceleration)

        # times_iomega should still work though - try to convert S1 to "power change"
        asd.times_iomega(1).unit.to("W/s/Hz(1/2)")

    def test_frequency_series_units_conversion_random_noise(self):
        # test if unit conversion makes sense: check values vs manual calculation
        # create random white noise displacement time series
        n_samples = 10000
        total_time = 1  # in seconds
        sampling_frequency = n_samples / total_time
        noise_scale = 10
        t = np.linspace(0, 1, n_samples)
        s1 = np.random.normal(0, scale=noise_scale, size=n_samples)
        displacement = TimeSeries(
            s1, sample_rate=sampling_frequency, name="Signal 1", unit="meter"
        )

        # calculate asd and convert
        displacement_asd = displacement.asd(method="lpsd")
        velocity_asd = displacement_asd.to_velocity()
        acceleration_asd = displacement_asd.to_acceleration()

        omega10 = displacement_asd.frequencies.value[10] * 2 * np.pi
        omega20 = displacement_asd.frequencies.value[20] * 2 * np.pi

        self.assertAlmostEqual(
            displacement_asd.value[10], velocity_asd.value[10] / omega10, delta=0.001
        )
        self.assertAlmostEqual(
            displacement_asd.value[10],
            acceleration_asd.value[10] / omega10 / omega10,
            delta=0.001,
        )

        self.assertAlmostEqual(
            displacement_asd.value[20], velocity_asd.value[20] / omega20, delta=0.001
        )
        self.assertAlmostEqual(
            displacement_asd.value[20],
            acceleration_asd.value[20] / omega20 / omega20,
            delta=0.001,
        )

    def test_is_asd(self):
        for unit in self.units:
            s1 = TimeSeries(
                self.s1, sample_rate=self.sample_rate, name="Signal 1", unit=unit
            )
            s1_asd = s1.asd(method="lpsd")
            s1_psd = s1.psd(method="lpsd")
            self.assertEqual(s1_asd.is_asd, True)
            self.assertEqual(s1_psd.is_asd, False)

    def test_is_psd(self):
        for unit in self.units:
            s1 = TimeSeries(
                self.s1, sample_rate=self.sample_rate, name="Signal 1", unit=unit
            )
            s1_asd = s1.asd(method="lpsd")
            s1_psd = s1.psd(method="lpsd")
            self.assertEqual(s1_psd.is_psd, True)
            self.assertEqual(s1_asd.is_psd, False)

    def test_rms(self):
        for unit in self.units:
            s1 = TimeSeries(
                self.s1, sample_rate=self.sample_rate, name="Signal 1", unit=unit
            )
            s1_asd = s1.asd(method="lpsd")
            s1_psd = s1.psd(method="lpsd")
            rms_from_asd = s1_asd.rms()
            rms_from_psd = s1_psd.rms()

            np.testing.assert_array_equal(rms_from_asd.value, rms_from_psd.value)
            self.assertAlmostEqual(rms_from_asd.value[133], 4.886362358383136)
            self.assertGreater(np.max(rms_from_asd.value), np.max(s1_asd.value))
            self.assertEqual(rms_from_asd.unit, unit)

    def test_rms_start_freq(self):
        s1 = TimeSeries(
            self.s1, sample_rate=self.sample_rate, name="Signal 1", unit="m"
        )
        s1_asd = s1.asd(method="lpsd")
        rms_from_asd = s1_asd.rms(100)
        self.assertGreaterEqual(100, np.max(rms_from_asd.frequencies.value))
        self.assertGreater(len(s1_asd), len(rms_from_asd))

    def test_rms_wrong_unit(self):
        s1 = TimeSeries(
            self.s1, sample_rate=self.sample_rate, name="Signal 1", unit="Hz2"
        )
        s1_asd = s1.asd(method="lpsd")
        self.assertRaises(ValueError, s1_asd.rms)

    def test_coherence_units(self):
        s1 = TimeSeries(self.s1, sample_rate=1 / 1000, unit="rad")
        s2 = TimeSeries(self.s2, sample_rate=1 / 1000, unit="rad")

        try:
            1 - s1.coherence(s2)
        except UnitConversionError as e:
            assert False, e

    def test_wf_1_reference(self):
        signal1 = TimeSeries(
            self.wf_s1,
            sample_rate=self.wf_sampling_frequency,
            name="Signal 1",
            unit="meter",
        )
        signal2 = TimeSeries(
            self.wf_s2,
            sample_rate=self.wf_sampling_frequency,
            name="Signal 2",
            unit="meter",
        )
        n_taps = 150
        n_samples_for_generation = (
            n_taps * 5 + 1
        )  # This is enough samples for 5 filters. All 5 will be
        # automatically generated and the best selected
        # 150-taps WF with a single reference for both frequencies:
        WF_1ref = WienerFilter(
            signal2[0:n_samples_for_generation],
            [signal1[0:n_samples_for_generation]],
            n_taps=n_taps,
            normalize_data=True,
            subtract_mean=False,
            verbose=True,
        )
        WF_1ref.create_filters()
        WF_1ref_sub_mean = WienerFilter(
            signal2[0:n_samples_for_generation],
            [signal1[0:n_samples_for_generation]],
            n_taps=n_taps,
            normalize_data=True,
            subtract_mean=True,
            verbose=True,
        )
        WF_1ref_sub_mean.create_filters()
        wf_1ref_signal2 = WF_1ref.apply(
            inputs_list=[
                signal1[n_samples_for_generation : n_samples_for_generation + 500]
            ],
            zero_padding=True,
        )
        wf_1ref_sub_mean_signal2 = WF_1ref_sub_mean.apply(
            inputs_list=[
                signal1[n_samples_for_generation : n_samples_for_generation + 500]
            ],
            zero_padding=True,
        )
        s2_peak = _max_value_in_window(
            signal2.value[
                n_samples_for_generation + 465 : n_samples_for_generation + 485
            ],
            np.arange(465, 485),
            475,
            10,
        )
        s2_wf_peak = _max_value_in_window(
            wf_1ref_signal2.value[465:485], np.arange(465, 485), 475, 10
        )
        s2_wf_sub_mean_peak = _max_value_in_window(
            wf_1ref_sub_mean_signal2.value[465:485], np.arange(465, 485), 475, 10
        )
        self.assertAlmostEqual(s2_peak, s2_wf_peak, delta=0.1)
        self.assertAlmostEqual(s2_peak, s2_wf_sub_mean_peak, delta=0.6)

    def test_wf_1_reference_with_norm_factor(self):
        signal1 = TimeSeries(
            self.wf_s1,
            sample_rate=self.wf_sampling_frequency,
            name="Signal 1",
            unit="meter",
        )
        signal2 = TimeSeries(
            self.wf_s2,
            sample_rate=self.wf_sampling_frequency,
            name="Signal 2",
            unit="meter",
        )
        n_taps = 150
        n_samples_for_generation = (
            n_taps * 5 + 1
        )  # This is enough samples for 5 filters. All 5 will be
        # automatically generated and the best selected
        # 150-taps WF with a single reference for both frequencies:
        WF_1ref = WienerFilter(
            signal2[0:n_samples_for_generation],
            [signal1[0:n_samples_for_generation]],
            n_taps=n_taps,
            normalize_data=False,
            subtract_mean=False,
            use_norm_factor=True,
            verbose=True,
        )
        WF_1ref.create_filters()
        wf_1ref_signal2 = WF_1ref.apply(
            inputs_list=[
                signal1[n_samples_for_generation : n_samples_for_generation + 500]
            ],
            zero_padding=True,
        )
        s2_peak = _max_value_in_window(
            signal2.value[
                n_samples_for_generation + 465 : n_samples_for_generation + 485
            ],
            np.arange(465, 485),
            475,
            10,
        )
        s2_wf_peak = _max_value_in_window(
            wf_1ref_signal2.value[465:485], np.arange(465, 485), 475, 10
        )
        self.assertAlmostEqual(s2_peak, s2_wf_peak, delta=0.1)

    def test_wf_2_references(self):
        signal11 = TimeSeries(
            self.wf_s11,
            sample_rate=self.wf_sampling_frequency,
            name="Signal 11",
            unit="meter",
        )
        signal12 = TimeSeries(
            self.wf_s12,
            sample_rate=self.wf_sampling_frequency,
            name="Signal 12",
            unit="meter",
        )
        signal2 = TimeSeries(
            self.wf_s2,
            sample_rate=self.wf_sampling_frequency,
            name="Signal 2",
            unit="meter",
        )
        n_taps = 150
        n_samples_for_generation = (
            n_taps * 5 + 1
        )  # This is enough samples for 5 filters. All 5 will be
        # automatically generated and the best selected
        # 150-taps WF with 2 references, one for each frequency:
        ts_dict = TimeSeriesDict()
        ts_dict["s1"] = signal11[0:n_samples_for_generation]
        ts_dict["s2"] = signal12[0:n_samples_for_generation]
        WF_2ref = WienerFilter(
            signal2[0:n_samples_for_generation],
            ts_dict,
            n_taps=n_taps,
            normalize_data=True,
            subtract_mean=False,
        )
        WF_2ref.create_filters()
        wf_2ref_signal2 = WF_2ref.apply(
            inputs_list=[
                signal11[n_samples_for_generation:],
                signal12[n_samples_for_generation:],
            ],
            zero_padding=True,
        )
        s2_peak = _max_value_in_window(
            signal2.value[
                n_samples_for_generation + 465 : n_samples_for_generation + 485
            ],
            np.arange(465, 485),
            475,
            10,
        )
        s2_wf_peak = _max_value_in_window(
            wf_2ref_signal2.value[465:485], np.arange(465, 485), 475, 10
        )
        self.assertAlmostEqual(s2_peak, s2_wf_peak, delta=1.5)

    def test_wf_save_load(self):
        signal1 = TimeSeries(
            self.wf_s1,
            sample_rate=self.wf_sampling_frequency,
            name="Signal 1",
            unit="meter",
        )
        signal2 = TimeSeries(
            self.wf_s2,
            sample_rate=self.wf_sampling_frequency,
            name="Signal 2",
            unit="meter",
        )
        n_taps = 150
        n_samples_for_generation = (
            n_taps * 5 + 1
        )  # This is enough samples for 5 filters. All 5 will be
        # automatically generated and the best selected
        # 150-taps WF with a single reference for both frequencies:
        WF_1ref = WienerFilter(
            signal2[0:n_samples_for_generation],
            [signal1[0:n_samples_for_generation]],
            n_taps=n_taps,
            normalize_data=True,
            subtract_mean=True,
            solver="cg",
        )
        WF_1ref.create_filters()
        wf_1ref_signal2 = WF_1ref.apply(
            inputs_list=[
                signal1[n_samples_for_generation : n_samples_for_generation + 500]
            ],
            zero_padding=True,
        )
        WF_1ref.save("test_WF")
        self.assertRaises(ValueError, WF_1ref.save, "C:\\blabla\\test")
        loaded_WF = WienerFilter.load("test_WF.json")
        loaded_WF = WienerFilter.load("test_WF")
        self.assertRaises(NotImplementedError, loaded_WF.create_filters)
        self.assertRaises(NotImplementedError, loaded_WF.determine_best_filter)
        self.assertRaises(ValueError, WienerFilter.load, "C:\\blabla\\test")
        self.assertRaises(NotImplementedError, loaded_WF.apply, to_training_data=True)
        wf_1ref_loaded_signal2 = loaded_WF.apply(
            inputs_list=[
                signal1[n_samples_for_generation : n_samples_for_generation + 500]
            ],
            zero_padding=True,
        )
        s2_wf_peak = _max_value_in_window(
            wf_1ref_signal2.value[465:485], np.arange(465, 485), 475, 10
        )
        s2_wf_loaded_peak = _max_value_in_window(
            wf_1ref_loaded_signal2.value[465:485], np.arange(465, 485), 475, 10
        )
        self.assertAlmostEqual(s2_wf_loaded_peak, s2_wf_peak)

    def test_wf_multiprocessing(self):
        signal1 = TimeSeries(
            self.wf_s1,
            sample_rate=self.wf_sampling_frequency,
            name="Signal 1",
            unit="meter",
        )
        signal2 = TimeSeries(
            self.wf_s2,
            sample_rate=self.wf_sampling_frequency,
            name="Signal 2",
            unit="meter",
        )
        n_taps = 150
        n_samples_for_generation = (
            n_taps * 5 + 1
        )  # This is enough samples for 5 filters. All 5 will be
        # automatically generated and the best selected
        # 150-taps WF with a single reference for both frequencies:
        WF_1ref = WienerFilter(
            signal2[0:n_samples_for_generation],
            [signal1[0:n_samples_for_generation]],
            n_taps=n_taps,
            normalize_data=True,
            subtract_mean=False,
        )
        WF_1ref.create_filters()
        wf_1ref_signal2 = WF_1ref.apply(
            inputs_list=[signal1[n_samples_for_generation:]],
            zero_padding=True,
            use_multiprocessing=True,
        )
        wf_1ref_signal2_dummy = WF_1ref.apply(
            inputs_list=[
                signal1[
                    n_samples_for_generation : n_samples_for_generation + n_taps + 1
                ]
            ],
            zero_padding=True,
            use_multiprocessing=True,
        )
        s2_peak = _max_value_in_window(
            signal2.value[
                n_samples_for_generation + 865 : n_samples_for_generation + 885
            ],
            np.arange(465, 485),
            475,
            10,
        )
        s2_wf_peak = _max_value_in_window(
            wf_1ref_signal2.value[465:485], np.arange(465, 485), 475, 10
        )
        self.assertAlmostEqual(s2_peak, s2_wf_peak, delta=0.1)

    def test_wf_no_zero_padding(self):
        signal1 = TimeSeries(
            self.wf_s1,
            sample_rate=self.wf_sampling_frequency,
            name="Signal 1",
            unit="meter",
        )
        signal2 = TimeSeries(
            self.wf_s2,
            sample_rate=self.wf_sampling_frequency,
            name="Signal 2",
            unit="meter",
        )
        n_taps = 150
        n_samples_for_generation = (
            n_taps * 5 + 1
        )  # This is enough samples for 5 filters. All 5 will be
        # automatically generated and the best selected
        # 150-taps WF with a single reference for both frequencies:
        WF_1ref = WienerFilter(
            signal2[0:n_samples_for_generation],
            [signal1[0:n_samples_for_generation]],
            n_taps=n_taps,
            normalize_data=True,
            subtract_mean=False,
        )
        WF_1ref.create_filters()
        wf_1ref_signal2 = WF_1ref.apply(
            inputs_list=[
                signal1[n_samples_for_generation : n_samples_for_generation + 500]
            ],
            zero_padding=False,
        )
        self.assertAlmostEqual(
            signal2.t0.value
            + (n_samples_for_generation + n_taps) / self.wf_sampling_frequency,
            wf_1ref_signal2.t0.value,
        )

    def test_wf_wrong_arguments(self):
        signal1 = TimeSeries(
            self.wf_s1,
            sample_rate=self.wf_sampling_frequency,
            name="Signal 1",
            unit="meter",
        )
        signal2 = TimeSeries(
            self.wf_s2,
            sample_rate=self.wf_sampling_frequency,
            name="Signal 2",
            unit="meter",
        )
        fast_signal2 = TimeSeries(
            self.wf_s2,
            sample_rate=self.wf_sampling_frequency * 2,
            name="Signal 2",
            unit="meter",
        )
        shifted_signal2 = TimeSeries(
            self.wf_s2,
            sample_rate=self.wf_sampling_frequency,
            name="Signal 2",
            unit="meter",
        )
        shifted_signal2.epoch = signal2.epoch + 1
        complex_signal2 = TimeSeries(
            self.wf_s2 + 1j * self.wf_s2,
            sample_rate=self.wf_sampling_frequency,
            name="Signal 2",
            unit="meter",
        )

        self.assertRaises(TypeError, WienerFilter, signal2)
        self.assertRaises(
            ValueError, WienerFilter, signal2, [signal1[100:]], n_taps=150
        )
        self.assertRaises(
            NotImplementedError,
            WienerFilter,
            signal2,
            [signal1],
            n_taps=150,
            solver="pink unicorn",
        )
        self.assertRaises(
            NotImplementedError,
            WienerFilter,
            signal2,
            [signal1],
            n_taps=150,
            use_norm_factor=True,
            normalize=True,
        )
        self.assertRaises(ValueError, WienerFilter, fast_signal2, [signal1], n_taps=150)
        self.assertRaises(
            ValueError, WienerFilter, shifted_signal2, [signal1], n_taps=150
        )
        self.assertRaises(
            NotImplementedError, WienerFilter, complex_signal2, [signal1], n_taps=150
        )
        self.assertRaises(ValueError, WienerFilter, signal2, [signal1], n_taps=-1)
        self.assertRaises(
            ValueError, WienerFilter, signal2, [signal1, signal1[100:]], n_taps=150
        )
        self.assertRaises(TypeError, WienerFilter, signal2, 0, n_taps=150)
        wf = WienerFilter(signal2[0:100], [signal1[0:100]], n_taps=150)
        self.assertEqual(wf.n_taps, 99)
        wf = WienerFilter(signal2[0:301], [signal1[0:301]], n_taps=150)
        wf.create_filters()
        self.assertEqual(len(wf.W.items()), 2)
        wf = WienerFilter(signal2[0:300], [signal1[0:300]], n_taps=150)
        wf.create_filters()
        self.assertEqual(len(wf.W.items()), 1)
        wf = WienerFilter(signal2[0:100], [signal1[0:100]], n_taps=150)
        wf.create_filters()
        self.assertRaises(
            ValueError, wf.apply, inputs_list=[signal1[0:100]], to_training_data=True
        )
        self.assertRaises(ValueError, wf.apply, inputs_list=[signal1[0:10]])
        self.assertRaises(ValueError, wf.apply, inputs_list=[fast_signal2[0:100]])
        self.assertRaises(
            ValueError, wf.apply, inputs_list=[signal1[0:100], signal1[0:100]]
        )
