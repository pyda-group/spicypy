"""
Class for 'control' module unit tests

Artem Basalaev <artem[dot]basalaev[at]pm.me>
Christian Darsow-Fromm <cdarsowf[at]physnet.uni-hamburg.de>,
Saurav Bania <saurav[dot]bania[at]studium.uni-hamburg.de>
"""
import unittest
import numpy as np
from control import tf, StateSpace, TransferFunction
from spicypy.control.utils import ellip_sus_z, ellip_sus, pairQ, zpkgf

from spicypy.signal import TimeSeries


class Test(unittest.TestCase):
    def setUp(self):
        self.sys_feedback = StateSpace([[-50.0]], [[1.0]], [[-50.0]], [[1.0]])
        self.sys_integrator = StateSpace(
            [[-0]],
            [[10.0]],
            [[5.0]],
            [[0.0]],
        )
        self.sys_mimo = StateSpace(
            [[-0, -0], [-0, -0]],
            [[10.0, 10.0], [10.0, 10.0]],
            [[5.0, 5.0], [5.0, 5.0]],
            [[0.0, 0.0], [0.0, 0.0]],
        )
        self.random_sys_2_2_2 = StateSpace(
            [[0.95451414, 13.62275813], [-0.98899356, -9.09470192]],
            [[-0.0, -1.18239992], [-0.0, 0.45243475]],
            [[-2.47411679, -0.49644232], [-0.0, 0.70283601]],
            [[0.0, 0.0], [-0.0, 0.0]],
        )
        self.random_sys_9_3_3 = StateSpace(
            [
                [
                    -3.05375816e00,
                    7.80757870e-01,
                    8.23287052e-01,
                    3.89629092e00,
                    2.85119110e00,
                    2.07152606e00,
                    6.65594435e-01,
                    -9.88471418e-01,
                    1.73740110e00,
                ],
                [
                    1.36013419e01,
                    -2.13396921e01,
                    -2.57050231e01,
                    -7.34970084e00,
                    -2.52727266e01,
                    -2.41758930e01,
                    1.68428433e01,
                    1.96759825e-01,
                    -1.45270589e01,
                ],
                [
                    2.24213941e00,
                    -2.06451504e00,
                    -4.96541133e00,
                    8.37633048e-01,
                    -3.47155695e00,
                    -2.81927575e00,
                    2.98628547e00,
                    -2.12268318e00,
                    -1.95758202e00,
                ],
                [
                    3.00068206e00,
                    -7.39107822e00,
                    -6.60462070e00,
                    -4.33562069e00,
                    -7.12516060e00,
                    -7.65255666e00,
                    4.09748024e00,
                    3.68114894e00,
                    -3.73233611e00,
                ],
                [
                    -1.98744215e01,
                    2.66172169e01,
                    2.97785264e01,
                    1.53509673e01,
                    3.20732230e01,
                    3.30264123e01,
                    -1.49638085e01,
                    -6.63820897e00,
                    1.94657668e01,
                ],
                [
                    -2.73903969e00,
                    4.07494838e00,
                    2.58939248e00,
                    3.54413667e00,
                    3.64171190e00,
                    3.72338909e00,
                    1.32982312e-01,
                    -1.94488950e00,
                    1.43125539e00,
                ],
                [
                    4.00494982e00,
                    -5.22505990e00,
                    -7.04795578e00,
                    -9.76501278e-01,
                    -6.31839237e00,
                    -4.69375003e00,
                    3.86360157e00,
                    -3.46244664e00,
                    -2.35868214e00,
                ],
                [
                    -1.42395153e00,
                    1.45959486e00,
                    1.04937736e00,
                    8.35688333e-01,
                    5.96452177e-01,
                    -7.72691992e-03,
                    2.30396570e00,
                    -1.27473203e00,
                    -1.87037339e00,
                ],
                [
                    1.20908546e01,
                    -1.60463629e01,
                    -1.91318840e01,
                    -8.77488668e00,
                    -2.05576943e01,
                    -2.00019288e01,
                    1.08284690e01,
                    2.40623817e00,
                    -1.25961933e01,
                ],
            ],
            [
                [-0.01631169, 0.69450448, 0.9763622],
                [1.15351189, -2.23911917, -0.59943916],
                [-0.04935018, -0.58599012, 0.0],
                [0.36443982, -1.72147885, 1.24368891],
                [0.0, -0.0, 1.1456946],
                [0.97162479, 0.56334884, -0.12967027],
                [-0.29579813, 0.0, -1.36350339],
                [-0.79264755, -0.32319302, -0.35947031],
                [-0.49910378, 0.03168619, -0.49742318],
            ],
            [
                [
                    0.50672665,
                    0.2709758,
                    -0.0,
                    0.58829473,
                    0.84080015,
                    -0.0,
                    0.54921916,
                    -0.93022034,
                    -0.0,
                ],
                [
                    0.2201862,
                    -0.80961073,
                    -0.93207889,
                    -0.79715643,
                    -0.0,
                    -0.0,
                    1.09827357,
                    -1.16782472,
                    -1.66037577,
                ],
                [
                    -0.0,
                    1.20233904,
                    0.42939532,
                    -0.0,
                    1.22199532,
                    -0.02858116,
                    0.0,
                    -0.91159036,
                    -0.72937929,
                ],
            ],
            [[0.0, -0.0, 0.0], [-0.0, 0.0, 0.0], [-0.0, 0.0, -0.0]],
        )

        pass

    def test_system_import(self):
        from spicypy.control.system import System

    def test_system_creation_from_tf(self):
        from spicypy.control import System

        g = tf(1, 1)
        # create a simple system
        System(g)

    def test_system_creation_from_ss(self):
        from spicypy.control import System

        sys = System(self.sys_feedback)
        self.assertEqual(sys.A, self.sys_feedback.A)
        self.assertEqual(sys.B, self.sys_feedback.B)
        self.assertEqual(sys.C, self.sys_feedback.C)
        self.assertEqual(sys.D, self.sys_feedback.D)

    def test_feedback_system(self):
        from spicypy.control.system import System

        s = tf("s")
        g = tf(1, 1)
        h = 50 / s

        # create a feedback system
        sys_plant = System(g)
        sys_ctrl = System(h)
        sys_feedback = sys_plant.feedback(sys_ctrl)
        try:
            self.assertEqual(sys_feedback.A, self.sys_feedback.A)
            self.assertEqual(sys_feedback.B, self.sys_feedback.B)
            self.assertEqual(sys_feedback.C, self.sys_feedback.C)
            self.assertEqual(sys_feedback.D, self.sys_feedback.D)
        except AssertionError:
            #  sometimes, randomly, python-control creates equivalent system with 10x factor at input and 0.1 at output
            self.assertEqual(sys_feedback.A, self.sys_feedback.A)
            self.assertEqual(sys_feedback.B, self.sys_feedback.B * 10)
            self.assertEqual(sys_feedback.C, self.sys_feedback.C * 0.1)
            self.assertEqual(sys_feedback.D, self.sys_feedback.D)

        self.assertIsInstance(sys_feedback, System)

    def test_control_system_response_siso(self):
        from spicypy.control.system import System

        t = np.linspace(0, 1, 1000)
        # sine wave
        s = 10 * np.sin(2 * np.pi * 50 * t)
        ts = TimeSeries(s, times=t, unit="m")

        # SISO system, tf=50/s: should attenuate signal
        resp_ts = System(self.sys_integrator).response(ts)
        self.assertLess(np.max(resp_ts.value), 3.20)

    def test_control_system_response_mimo(self):
        from spicypy.control.system import System

        t = np.linspace(0, 1, 1000)
        # sine wave
        s = 10 * np.sin(2 * np.pi * 50 * t)
        ts1 = TimeSeries(s, times=t)
        ts2 = TimeSeries(2 * s, times=t)

        # now test MIMO (2 inputs, 2 outputs)
        resp_ts = System(self.sys_mimo).response([ts1, ts2])
        # expect same outputs
        self.assertEqual(resp_ts[0].value[10], resp_ts[1].value[10])
        # known expected difference (signal amplification)
        self.assertLess(abs(np.max(ts1.value) - np.max(resp_ts[0].value)), 9.4)
        # test response with only one signal
        System(self.sys_mimo).response([ts1, None])
        # check that error is thrown if more signals than inputs
        with self.assertRaises(ValueError):
            System(self.sys_mimo).response([ts1, ts2, ts2])
        # check that error is thrown if only None signals are specified
        with self.assertRaises(ValueError):
            System(self.sys_mimo).response([None, None])

    def test_python_control_wrapping(self):
        from spicypy.control.system import System

        system_mimo = System(self.sys_mimo)
        system_mimo_rand = System(self.random_sys_2_2_2)
        sys_add = system_mimo + system_mimo_rand
        self.assertIsInstance(sys_add, System)
        sys_slice = system_mimo[0, 0]
        self.assertIsInstance(sys_slice, System)
        sys_mul = system_mimo * system_mimo_rand
        self.assertIsInstance(sys_mul, System)
        sys_neg = -system_mimo
        self.assertIsInstance(sys_neg, System)
        sys_rmul = system_mimo.__rmul__(2.0)
        self.assertIsInstance(sys_rmul, System)
        sys_radd = system_mimo.__radd__(2.0)
        self.assertIsInstance(sys_radd, System)
        sys_rsub = system_mimo.__rsub__(system_mimo_rand)
        self.assertIsInstance(sys_rsub, System)
        sys_sub = system_mimo - system_mimo_rand
        self.assertIsInstance(sys_sub, System)
        sys_div = system_mimo / 2.0
        self.assertIsInstance(sys_div, System)
        sys_app = system_mimo.append(system_mimo_rand)
        self.assertIsInstance(sys_app, System)
        sys_copy = system_mimo.copy()
        self.assertIsInstance(sys_copy, System)
        sys_lin = system_mimo.linearize(x0=[0.0, 0.0], u0=[0.0, 0.0])
        self.assertIsInstance(sys_lin, System)
        try:
            sys_min = system_mimo.minreal()
            self.assertIsInstance(sys_min, System)
        except TypeError:
            # This test requires Slycot, which is optional. When no Slycot available, raises TypeError
            # Won't bother adding it in pipeline, so it's skipped with no Slycot
            pass

    def test_add_controller_argument_lengths(self):
        from spicypy.control.system import System

        sys = System(self.random_sys_9_3_3)
        controller = [tf([0, 0], 1), tf([0, 0], 1)]
        controller_names = ["filter_1", "filter_2"]
        feedin = [0, 2]
        feedout = [0, 1, 2]

        with self.assertRaises(ValueError):
            sys.add_controller(
                controller, feedin, feedout, control_names=controller_names
            )

    def test_add_controller_control_names_length(self):
        from spicypy.control.system import System

        sys = System(self.random_sys_9_3_3)
        controller = [tf([0, 0], 1), tf([0, 0], 1)]
        controller_names = ["filter_1"]
        feedin = [0, 2]
        feedout = [0, 1]

        with self.assertRaises(ValueError):
            sys.add_controller(
                controller, feedin, feedout, control_names=controller_names
            )

    def test_add_controller_negative_feedback(self):
        from spicypy.control.system import System

        sys = System(self.random_sys_9_3_3)
        controller = [tf([0, 0], 1), tf([0, 0], 1)]
        controller_names = ["filter_1", "filter_2"]
        feedin = [0, 2]
        feedout = [0, 1]

        with self.assertRaises(ValueError):
            sys.add_controller(
                controller,
                feedin,
                feedout,
                control_names=controller_names,
                negative_feedback=2,
            )

    def test_add_controller_non_TF_in_controller(self):
        from spicypy.control.system import System

        sys = System(self.random_sys_9_3_3)
        controller = [tf([0, 0], 1), 5]
        feedin = [0, 2]
        feedout = [0, 1]

        with self.assertRaises(ValueError):
            sys.add_controller(controller, feedin, feedout)

    def test_add_controller_improper_TF_in_controller(self):
        from spicypy.control.system import System

        sys = System(self.random_sys_9_3_3)
        controller = [tf([1, 1], 1), tf([0, 0], 1)]
        feedin = [0, 2]
        feedout = [0, 1]

        with self.assertRaises(ValueError):
            sys.add_controller(controller, feedin, feedout)

    def test_add_controller_state_matrix(self):
        from spicypy.control.system import System

        sys = System(self.random_sys_2_2_2)
        controller = [tf(1, 1), tf(1, 1)]
        feedin = [0, 1]
        feedout = [0, 1]

        connected_system = sys.add_controller(
            controller, feedin, feedout, negative_feedback=False
        )

        self.assertIsInstance(connected_system, System)
        self.assertEqual(sys.ninputs, connected_system.ninputs)
        self.assertEqual(sys.noutputs, connected_system.noutputs)

        # note: matrices A (state matrix) will be slightly different - confirm if it's ok?
        # note2: matrices D are zero in both cases so no need to compare
        np.testing.assert_array_almost_equal(connected_system.B, sys.B)
        np.testing.assert_array_almost_equal(connected_system.C, sys.C)

    def test_add_controller_impulse_response(self):
        from spicypy.control.system import System
        from control import impulse_response

        s = tf("s")
        # passive isolator transfer function
        f0 = 20.0  # resonance frequency in HZ
        D = 1 / (s**2 + 50 * s + (2 * np.pi * f0) ** 2)
        # controller (simple integrator)
        C = 10**-3 / s

        plant = System(D, name="P")
        controller = C
        connected_system = plant.add_controller(
            [controller], [0], [0], negative_feedback=True, control_names=["C"]
        )
        # test against a "traditional" feedback method
        feedback_system = plant.feedback(controller)
        ir_feedback = impulse_response(feedback_system).outputs
        ir_connected = impulse_response(connected_system).outputs
        np.testing.assert_array_almost_equal(ir_feedback, ir_connected)

    def test_pairQ_output(self):

        f0 = 1
        Q = 1

        result = pairQ(f0, Q)

        self.assertIsInstance(result, list)
        self.assertEqual(len(result), 2)
        self.assertIsInstance(result[0], complex)
        self.assertIsInstance(result[1], complex)

    def test_pairQ_input_types(self):

        f0 = 1
        Q = 2

        with self.assertRaises(ValueError):
            pairQ(f0 + 1j, Q)

        with self.assertRaises(ValueError):
            pairQ(f0, Q + 1j)

    def test_zpkgf_output(self):

        zeros = [1, 2]
        poles = [1, 2]
        gain = 5
        gfreq = 10

        result = zpkgf(zeros, poles, gain, gfreq)

        self.assertIsInstance(result, TransferFunction)

    def test_zpkgf_inputs(self):

        zeros = [1, 2]
        poles = [3, 4]
        gain = 5
        gfreq = 10

        with self.assertRaises(ValueError):
            zpkgf(zeros[0], poles, gain, gfreq)

        with self.assertRaises(ValueError):
            zpkgf(zeros, poles[1], gain, gfreq)

        with self.assertRaises(ValueError):
            zpkgf(zeros, poles, gain + 1j, gfreq)

        with self.assertRaises(ValueError):
            zpkgf(zeros, poles, gain, gfreq + 1j)

    def test_ellip_sus_output(self):

        freq = 1
        order = 2
        ripple = 3
        stopDB = 4

        result = ellip_sus(freq, order, ripple, stopDB)

        self.assertIsInstance(result, TransferFunction)

    def test_ellip_sus_inputs(self):
        from spicypy.control.utils import ellip_sus

        freq = 1
        order = 2
        ripple = 3
        stopDB = 4

        with self.assertRaises(ValueError):
            ellip_sus(freq + 1j, order, ripple, stopDB)

        with self.assertRaises(ValueError):
            ellip_sus(freq, order + 1j, ripple, stopDB)

        with self.assertRaises(ValueError):
            ellip_sus(freq, order, ripple + 1j, stopDB)

        with self.assertRaises(ValueError):
            ellip_sus(freq, order, ripple, stopDB + 1j)

    def test_ellip_sus_z_output(self):

        freq = 1
        order = 2
        ripple = 3
        stopDB = 4
        zQ = 5

        result = ellip_sus_z(freq, order, ripple, stopDB, zQ)

        self.assertIsInstance(result, TransferFunction)

    def test_ellip_sus_z_inputs(self):

        freq = 1
        order = 2
        ripple = 3
        stopDB = 4
        zQ = 5

        with self.assertRaises(ValueError):
            ellip_sus_z(freq + 1j, order, ripple, stopDB, zQ)

        with self.assertRaises(ValueError):
            ellip_sus_z(freq, order + 1j, ripple, stopDB, zQ)

        with self.assertRaises(ValueError):
            ellip_sus_z(freq, order, ripple + 1j, stopDB, zQ)

        with self.assertRaises(ValueError):
            ellip_sus_z(freq, order, ripple, stopDB + 1j, zQ)

        with self.assertRaises(ValueError):
            ellip_sus_z(freq, order, ripple, stopDB, zQ + 1j)

    def test_filter_creation(self):

        startramp = 10
        endramp = 20
        steps = 5
        dampie = 2
        ministep = zpkgf(
            pairQ(startramp, dampie), pairQ(startramp / np.sqrt(steps), dampie), 1, 0.01
        )
        prestep = zpkgf([0], [], 2e2, 1.8)
        ellip_rolloff = ellip_sus_z(5, 2, 1, 10, 3)

        tiny_damp_L = ellip_rolloff * ministep * prestep

        expected_den = [
            1.00000000e00,
            4.04176343e01,
            2.36741707e03,
            3.77826964e04,
            9.53315366e05,
        ]

        expected_num = [
            1.11842302e00,
            7.63991742e01,
            9.51752999e03,
            2.82463975e05,
            1.50249382e07,
            0.00000000e00,
        ]

        np.testing.assert_allclose(tiny_damp_L.num[0][0], expected_num)
        np.testing.assert_allclose(tiny_damp_L.den[0][0], expected_den)
