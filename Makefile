BRANCH ?= main
DOCKER ?= docker
PACKAGE = spicypy
DOCKER_IMAGE = gwdiexp/${PACKAGE}:${BRANCH}
OLD_PY_VERSION = 3.10
NEW_PY_VERSION = 3.11
EXECUTE_NOTEBOOKS ?= 0

all: mypy test package

test:
	PYTHONPATH=`pwd` poetry run py.test

test-docker:
	${DOCKER} run -v `pwd`:/code --rm -it ${DOCKER_IMAGE} make test

test-docker-old:
	${DOCKER} run -v `pwd`:/code --rm -it ${DOCKER_IMAGE}-${OLD_PY_VERSION} make test

mypy:
	poetry run mypy ${PACKAGE}

pylint:
	poetry run pylint ${PACKAGE}

black:
	poetry run black ${PACKAGE} tests

package:
	poetry build

upload:
	poetry config pypi-token.pypi ${POETRY_PYPI_TOKEN_PYPI}
	poetry publish

doc:
	@cd doc; poetry run make html

doc-docker:
	${DOCKER} run -v `pwd`:/code -e EXECUTE_NOTEBOOKS=${EXECUTE_NOTEBOOKS} -e PYTHONPATH=${PWD} --rm -it ${DOCKER_IMAGE} bash -c "poetry install && make doc"

docker-bash:
	${DOCKER} run -v `pwd`:/code -e EXECUTE_NOTEBOOKS=${EXECUTE_NOTEBOOKS} -e PYTHONPATH=${PWD} --rm -it ${DOCKER_IMAGE} bash

docker:
	${DOCKER} build . -f docker/Dockerfile -t ${DOCKER_IMAGE} \
		--build-arg PYTHON_VERSION=${NEW_PY_VERSION}
	${DOCKER} build . -f docker/Dockerfile -t ${DOCKER_IMAGE}-old \
		--build-arg PYTHON_VERSION=${OLD_PY_VERSION}

docker-push:
	${DOCKER} login
	${DOCKER} push ${DOCKER_IMAGE}
	${DOCKER} push ${DOCKER_IMAGE}-old

clean:
	@rm -r dist/

.PHONY: mypy test package docker doc
