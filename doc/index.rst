========
Spicypy
========

Signal processing & control systems. Combining several tools to facilitate signal processing, control systems modelling,
and the interface between the two.

Spicypy is an open-source project hosted on Gitlab: https://gitlab.com/pyda-group/spicypy.

Contents
========

.. toctree::
  :maxdepth: 2
  :caption: Usage

  Introduction <intro>
  Installation <install>
  Example 1: Feed a signal through a simple control system <examples/feed_signal_into_control_sys>
  Example 2: Time-match signals and estimate coherence <examples/match_signals_estimate_coherence>
  Example 3: Compare averaging methods for spectrum calculation (ASD, PSD, CSD) <examples/compare_averaging_methods>
  Example 4: Percentile calculation for the PSD/ASD (a measure of spectral uncertainty) <examples/percentile_example_seismic>
  Example 5: Differentiate and integrate frequency series to convert units; calculate RMS <examples/convert_between_displacement_velocity_acceleration>
  Example 6: Subtraction of coherent signals (Huddle test) <examples/subtract_coherent_signals>
  Example 7: Transfer function between two time series with different FFT averaging methods <examples/transfer_function>
  Example 8: Multiple-input single-output Wiener filter for noise subtraction <examples/wiener_filter>
  Example 9: Plot Einstein Telescope alignment sensing noise and make bode plots <examples/ET_alignment_sensing_noise>
  Example 10: Model active seismic isolation ("VATIGrav" platform) <examples/model_active_seismic_isolation_VATIGrav>
  Example 11: Import state space matrices, add controller, apply signal to specific input <examples/import_state_space_matrices_and_apply_seismic_motion_to_specific_input>

.. toctree::
  :maxdepth: 2
  :caption: API Reference

  Spicypy API <autoapi/spicypy/index>

.. toctree::
  :maxdepth: 2
  :caption: More

  Authors <authors>

.. toctree::
  :maxdepth: 2
  :caption: Development

  Developer's guide <guide>

  Development Environment (quick introduction for experts) <dev_install>

.. toctree::
  :maxdepth: 2
  :caption: Gitlab project

  https://gitlab.com/pyda-group/spicypy
