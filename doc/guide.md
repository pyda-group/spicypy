# Developer's Guide



You have an idea how to improve Spicypy, and in fact you implemented your changes already.
You even tested them in a Jupyter notebook and it all works, so how do you add it quickly?
(If you are actually first reading this before doing anything - great! The steps are the same anyway.)

In Spicypy we take time to review things,
which includes automatic checks of code syntax, formatting, and running this code with unit tests.
It also includes someone looking into your code structure and making sure that it conforms to
Spicypy paradigm: where possible, rely on underlying packages `GWpy` and `python-control`
(don't reinvent the wheel), and where not possible, implement new functionality with
the structure maximally similar to that of underlying packages, such that your changes have a chance
to propagate upstream into one of these packages one day.

But you don't have to redo everything from scratch. We can work on making your changes
conform to all of this step by step.

## First steps

### Creating an Issue and submitting a merge request

Most likely, if you did not create a separate branch already, you've added your changes to
the `main` branch. That will not work, because nobody is allowed to push into `main` (including
maintainers of the package). This is on purpose, such that everyone has to create a
[merge request](https://docs.gitlab.com/ee/user/project/merge_requests/), _document_ their intentions
and open the code for review by others.

First, stash any changes you did already (skip this step if you did not yet change anything). Don't
worry, all changes will be saved and we get them back soon. After you did `git add` all the
relevant files, do:
```
git stash
```


Now you should open a new Issue. "Issue" is a Gitlab-specific term; you can think of it as a ticket
which documents what the problem was that needed to be solved (with your changes) or the
intended improvement. You can first look at some [examples here.](https://gitlab.com/pyda-group/spicypy/-/issues)
Then open a new Issue, make sure to come up with a short but descriptive name, e.g. "Add transfer function
calculation", but not "Some improvements". In the description write what is wrong (if you are fixing a bug)
and what you want to do in plain English. Just as if you had to explain it to your colleague in five minutes,
it does not have to be too detailed.

Once you created an Issue you will see a button "Create merge request" - use it.

![Creating a merge request](_static/images/issue_create_mr.png)

By default, it will be named "Draft: Resolve [your issue name]". You can leave it like this for now.
Keep in mind that later on you will have to remove "Draft" from the name for it to be merged.
This is simply to prevent unfinished work from being accidentally merged, therefore you (the author)
decide when it's ready and remove "Draft" from the name. That does not mean it will be
merged right away though; a few more iterations may be required ;)

The description of the merge request is even more important. There you should describe where you added new
code and what it does. By default, it only says "Closes #[issue_number]". It's fine though if you
don't yet have a clear idea of the final structure, as it may change during the review process anyway.
So you can leave it now as it is and create the merge request. Here's an example of a mature
merge request description:

![Description of a merge request](_static/images/mr_description.png)

What you should also note in the newly created merge request is the name of your new branch (`tf_estimate_utilities`
above). This is the branch where your changes should go initially.

### Putting your changes to a new branch

If you're not yet there, switch to `main`, then pull:
```
git checkout main
git pull
```
Now switch to your new branch, the name of which you know from the previous step:
```
git checkout my_branch
```

Now you can bring back your changes from the stash:
```
git stash pop
```
Or, if you did not yet change anything, _this_ is where you start! After you are done with
your changes, do the usual
```
git add <modified file(s)>
git commit
git push
```
Now your changes should be successfully pushed to your branch, and you can nicely see the difference
wrt `main` in "Changes" tab of your merge request.
Go check it out in [merge requests](https://gitlab.com/pyda-group/spicypy/-/merge_requests).

Oh, and now you'll get an email that says "pipeline failed"...
Actually all that is described so far
is a bare minimum and only a starting point. There's some more work to do! :(
Now we need to satisfy all the automatic checks, which
will then fix the pipeline (and before it's fixed, nothing can be merged). Let's discuss this in detail
in the next section.

## Pipeline horrors

If you've completed the steps above, you might have noticed the following
"pipeline failed" icon below your merge request description:

![Failed pipeline](_static/images/pipeline_failed.png)

_Pipeline_ is a gitlab-specific term which signifies a script that runs
each time new code is pushed. There are several steps (stages) in it, and each performs
a specific set of checks or a task. If any of them fails, you get this "pipeline failed"
message and the code cannot be merged until the code is fixed and it passes all the steps.

By clicking on a link starting with `#`, you can see the details. A typical failed pipeline would
look like this:

![Failed pipeline: stages view](_static/images/pipeline_stages.png)

The left-most side is typically the most important here (but depends on
what you change exactly): these are checks of your code. In the middle and right parts,
the documents are compiled and the code is packaged. Here's what each stage on the left is doing:
* [`black`](https://black.readthedocs.io/en/stable/): a helper package that checks source code file formatting,
including tabulations and new lines. This might seem like over-kill, but it's important
to keep all our code formatting uniform for two main reasons: (1) it makes it easier to read
and therefore less prone to mistakes, and (2) there are no "spurious" changes added if you use
an editor that uses a different convention for new lines, for instance.
* [`mypy`](http://mypy-lang.org/):  a helper package which checks that source code has
static typing, or in other words explicitly defined variable types. Python's great advantage
is that it's a dynamically typed language, but sometimes static typing helps avoid mistakes.
For example when you assume your inputs are one specific type, but you actually get a different
one at run-time.
In some of these cases the code will still run but will produce unexpected results.
* [`pylint`](https://pylint.pycqa.org/en/latest/): a helper package that checks code syntax
(static code analyzer). Helps to find mistakes before even running the code. Note that
this one is not enforced, therefore if it produces some warnings, an exclamation mark will be
displayed as in the case above. It is nontheless strongly suggested to fix all its warnings.
* test: this is a step at which [unit tests](https://en.wikipedia.org/wiki/Unit_testing) are run.
These are self-sufficient short code pieces
that execute small parts of the code. It is necessary to create some for your code as well, more
on that below.

Now let's go step-by-step through the steps that fail most often and how to fix them.

### Stage "black"

If you click on the "black" stage above, you will see terminal output, probably something like this:

![Black failed](_static/images/black_failed.png)

Information after the "oh no" line tells us that `black` would reformat some files, in this case three files.
It can indeed fix everything automatically, but there's one step for you to do before committing your changes to
basically let it do its work.

The `Spicypy` development environment is best managed with
[`poetry`](https://python-poetry.org/). Install it on your system and then, inside the `spicypy` folder, do
```
poetry install
```
This will not only install `spicypy` package inside a safe isolated environment
(where you can experiment however you want), but will also install all _development dependencies_ (helper packages
that check code) automatically.

Now that you have [`pre-commit`](https://pre-commit.com/) installed you should do the following in root `spicypy`
folder after you changed any files _and_ did `git add` on them, but _before_ committing:
```
poetry run pre-commit
```
(In case you just had the pipeline failing with `black` on some files, you obviously haven't
done that before committing. Just add a newline somewhere in all files that `black`
complained about, `git add` them and run `pre-commit` as above.)

Now if you do `git status` you will see that some files were modified by `black`. Do `git add`
on them again, and now you can do `git commit` and `git push`. That should fix `black` stage
in the pipeline!

### Stage "pylint"

As mentioned above, `pylint` will not fail the pipeline but please do check and address its
warnings nonetheless. The pipeline stage output may look something like this:

![Output of pylint](_static/images/pylint.png)

In this example the first warning "No name 'fft' in module 'scipy'" may have been an actual
mistake that slipped uncaught because the piece of code with import _wasn't executed_.
The remaining two will not, per se,
prevent code from running, but the suggested fixes are reasonable to improve readability and
avoid potential mistakes in the future. Note that the warning about unused variable can often help
uncover a mistake, if the variable is actually intended to be used.

Sometimes, though, pylint warnings are way too stringent and not flexible. In some situations
it can be justified to ignore some of them, and in that case it's better to "tell" `pylint`
explicitly not to check certain lines. That is done with an inline comment like this:
```python
useless_var = 1  # pylint: disable=unused-variable
```
Use this feature with caution, not in places which could actually be improved.

Of course, it is possible to check `pylint` output locally, simply do:
```
poetry run pylint spicypy
```

### Unit tests (stage "test")

[Unit tests](https://en.wikipedia.org/wiki/Unit_testing) are small self-sufficient pieces of code
that combine the following features:
* Test each class/method/function separately (ideally, sometimes not possible to separate)
* Using small pre-defined inputs (create some arrays with data = fine, megabytes or more
of input data = to be avoided)
* Create a _meaningful_ test. E.g. imagine you want to test a PSD algorithm. Create some inputs
for which you know which frequencies _should_ be in the PSD, and check if you see what you expect.

First, let's take a look if you broke some of the existing tests with your changes. Navigate
to the respective stage in the pipeline, and you might see something like this:

![Failed unit test](_static/images/broken_test.png)

The lines above will hint at where the problem is and what it is exactly.
Unit tests live in the folder `spicypy/tests`. There you can see python files where tests _per module_
are collected, e.g. `test_signal.py`, `test_control.py`. You can try to run the tests locally
of course to get a better idea of what's wrong, e.g. by adding some printouts in the failing test.
Here's how:


```
make test
```

On Windows that sometimes would not work due to different handling of environment variables, in that case try:

```
poetry run py.test
```

Once you've fixed existing unit tests, you may still have the test stage failing, with a message
which looks like this:

![Example of insufficient coverage by unit tests](_static/images/coverage_fail.png)

This shows you break-down of fractions of code _covered_ by unit tests (hence called coverage).
Where some code is not fully covered (coverage less than 100%), it will show you line numbers
on the right for the uncovered parts. Finally, here it shows failure "Required test coverage
of 85% is not reached". This is to be expected, because we have this threshold of 85% and
you just added some new code, so the coverage slipped below 85%. To fix it, you should
obviously add more unit tests. But even if the coverage did not actually drop below 85%, you
should still write unit tests of your code ;) Any untested piece of code is a potential mistake missed.
A couple of tips on how to do it is below.

But first let's look at another tool which helps you better understand for which parts of your
code you should add unit tests. If you navigate to your merge request and go to the "Changes"
you may see something like this:

![Coverage displayed in "Changes" tab of a merge request](_static/images/coverage_in_mr.png)

Note these green and red lines on the left. With green it shows parts already covered by unit
tests, and with red - not covered. In this specific example there was no test with invalid
inputs, therefore the code branch with `ValueError` is never executed. And yes, you should
test not only "normal" conditions, but also invalid inputs and how your code handles them.

Now let's look at this example of a unit test:
```python
def test_daniell_csd_11_averages(self):

    t = np.arange(10000)
    # define two waves, each with two frequencies, one of which is the same (f1)
    f1 = 0.01
    f2 = 0.1
    f3 = 0.02

    a = 20 * np.sin(2 * np.pi * f1 * t) + 10 * np.sin(2 * np.pi * f2 * t)
    b = 20 * np.sin(2 * np.pi * f1 * t) + 10 * np.sin(2 * np.pi * f3 * t)

    csd = TimeSeries(a, times=t).csd(
        TimeSeries(b, times=t), method="daniell", number_averages=11
    )
    csd_default = TimeSeries(a, times=t).csd(TimeSeries(b, times=t))

    # check f1 - the common frequency
    max_csd = _max_value_in_window(csd.value, csd.frequencies.value, f1)
    max_csd_default = _max_value_in_window(
        csd_default.value, csd_default.frequencies.value, f1
    )

    np.testing.assert_allclose(max_csd, max_csd_default, rtol=0.87)
```

This is one of the tests of CSD algorithms. Note that first some inputs are defined - two
sine waves with specific frequencies - for which we know what we should expect as a result in CSD.
Specifically, we know that the CSD should have a peak at frequency `f1`. We could run this code once
and then in the test compare CSD at this frequency to the value we expect. Instead, in this
case the comparison is with the `default`
CSD calculation algorithm. Whether it's two algorithms, or a comparison to numerical values, you can use this
helpful `numpy` method:
```python
numpy.testing.assert_allclose
```
This lets you check if two arrays (or numbers) are close within certain tolerance level which is
set by the `rtol` parameter. There's a similar a bit more stringent check:
```python
numpy.testing.assert_almost_equal
```
And finally a check where you expect the numbers to be exactly the same:
```python
numpy.testing.assert_equal
```

But what if you want to check that your code fails for incorrect inputs?
In that case you want the test to _succeed_ if code fails. This is how it could be done:
```python
def test_invalid_transfer_function(self):

    a1 = [2, 2, 3, -1, 5]
    a2 = [4, 6, 8, -3, 6]
    t = [0, 1, 2, 3, 4]

    ts1 = TimeSeries(a1, times=t)
    ts2 = TimeSeries(a2, times=t)
    tf = TransferFunction(ts1, ts2)
    self.assertRaises(ValueError, TransferFunction, ts1, ts2, tf=tf.tf)
    self.assertRaises(ValueError, TransferFunction, ts1)
```
In the example above if the code raises `ValueError` the test will succeed, which is achieved
with `assertRaises` method.

Now this should be sufficient to get you started with unit tests, and of course you can use
some other functionality of `assert` and equality checks not mentioned here. Just make sure
that your unit tests are meaningful. If the code is executed by a unit test, the coverage and
pipeline will pass, but if the inputs were meaningless, it may not catch actual mistakes. Example:
 a PSD algorithm runs, but produces completely wrong output.

Often it might actually make sense to implement your unit tests when your changes have already undergone a few
iterations of collaborative work (see next section) and the structure of the code is more or less fixed.
It will be easier than to change all of the unit tests, because they typically assume a specific code
structure (e.g. test specific methods/functions).


## Collaborative work

Now that you fixed the pipeline (or at least did everything up to writing your unit tests),
and you think your changes are in good enough shape to be merged
(you removed "Draft:" from merge
request name), there might still be some more work to do. Please follow the discussion in your merge
request and respond there as well. It is strongly preferred to emails/separate discussions elsewhere,
not only because it is more easily accessible and preserved for history, but also because it's
quite convenient to discuss code by adding a comment to a specific line in your changes, and even
direct code suggestions. It can look like this, for example:

![Comment in a merge request with a suggested code change](_static/images/suggested_change.png)

By the way you don't have to agree with all the suggestions. If you think you have a better idea,
please make sure to communicate in the merge request and make your intentions clear as you
proceed with more changes. Also, any developer can check out and directly contribute in your
branch and vice versa, but it is polite to ask first if it's ok to do so, especially if it
changes substantially
the work that someone else has done (but don't worry too much about this, every commit is preserved
and we can always roll back any changes, at least before they are merged).

Good luck with your changes getting merged, and thanks for your contribution!

And if you find a mistake in this guide, or something is not clear or missing, please contact us,
and we'll do our best to fix it.
