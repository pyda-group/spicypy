# Setup a Development Environment

## Poetry

For package management of the development environment and dependencies we use [Poetry](https://python-poetry.org/).
It can be installed with
```bash
pip3 install poetry
```
and installs all required packages for development using:
```bash
poetry install
```
To activate the virtual environment use:
```bash
poetry shell
# or for a single command
poetry run command
```

## Code Formatter

We use [`pre-commit`](https://pre-commit.com/#python) for automatic code formatting before committing.
It is automatically installed with the development packages.
The command to enable the hooks is:
```bash
pre-commit install
```

## EditorConfig

Please make sure to have an editor (or a plugin) that supports [EditorConfig](https://editorconfig.org/).
It will avoid many unnecessary formatting problems.

## PyTest

We are using [pytest](https://github.com/pytest-dev/pytest) to run our Python unittests.
It is a bit further simplified by the command:
```bash
make test
```

## Sphinx docs

The documentation is generated with [Sphinx](https://www.sphinx-doc.org/).
It automatically generates the API documentation and is used to generate additional docs like this page.
The docs are compiled to html with
```bash
make doc
```
and the result can be found in `doc/_build/html`.
Additionally, the Jupyter notebooks from `examples` are automatically converted to reStructuredText to be included in the docs.
Add them to `index.rst` to include in the docs, but without file extension.

## Container

Currently we are using [`Docker` containers](https://www.docker.com/resources/what-container) to create the image for the testing environment.
The definition is in `docker/Dockerfile` and it is used in some `Makefile` commands containing the word `docker`.

## Version Numbering

For version numbering standards see [here](https://packaging.python.org/guides/distributing-packages-using-setuptools/?highlight=beta#standards-compliance-for-interoperability).
