#!/usr/bin/env bash
set -e
if [ "$EXECUTE_NOTEBOOKS" = 1 ]; then
  execute="--execute"
else
  execute=""
fi
echo EXECUTE_NOTEBOOKS: $EXECUTE_NOTEBOOKS
jupyter nbconvert ../examples/*.ipynb $execute --to rst --output-dir=examples
