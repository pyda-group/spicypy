# Introduction

Spicypy is based on two main packages: `GWpy` for signal-processing and `python-control` for control systems modelling.
The goal is to reuse as much existing functionality of these packages as possible.
Main Spicypy classes directly inherit from respective classes of these packages making all corresponding functionality available.

New features are added as additional methods. These features include interfacing signal processing (`GWpy`-like) classes with control system (`python-control`-like) classes.
This allows to seamlessly propagate signals through control systems and analyze the results.
Other requested features and progress in their implementation can be seen in [Issues](https://gitlab.com/pyda-group/spicypy/-/issues).
Current code is described in "API Reference" section.
