# spicypy

Signal processing & control systems. Combining several tools to facilitate signal processing, control systems modelling,
and the interface between the two.

More details are provided in [Documentation](https://pyda-group.gitlab.io/spicypy/).

# Development roadmap

Spicypy is based on two main packages: [GWpy](https://gwpy.github.io/docs/) for
signal-processing and [python-control](https://python-control.readthedocs.io/en/latest/)
for control systems modelling. The goal is to reuse as much of existing functionality of
these packages as possible.
Novel functionality to be implemented is discussed in
[Issues](https://gitlab.com/pyda-group/spicypy/-/issues).

Development process is steered by regular [meetings](https://gitlab.com/pyda-group/spicypy/-/wikis/home/meetings)
with participants across the gravitational wave science community.

The project is open for contributions, and feedback on usage is welcome. For reporting bugs and specific code suggestions
please consider [creating an Issue](https://gitlab.com/pyda-group/spicypy/-/issues/new?issue%5Bmilestone_id%5D=).
For other enquiries contact details are provided below.

# Contact

* Artem Basalaev (artem.basalaevATpmDOTme)
* Christian Darsow-Fromm (cdarsowfATphysnetDOTuni-hamburg.de)
* Oliver Gerberding (oliver.gerberdingATphysikDOTuni-hamburg.de)
* Martin Hewitson (martin.hewitsonATaeiDOTmpg.de)

# Cite

If you wish to reference Spicypy in your publication, please use citation provided here: [https://doi.org/10.5281/zenodo.10033637](https://doi.org/10.5281/zenodo.10033637).

# License

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
